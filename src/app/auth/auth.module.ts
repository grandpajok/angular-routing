import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { AuthComponent } from './auth.component';
import { AuthService } from './auth.service';
import { AuthGuard } from './auth.guard';
import { RouterModule } from '@angular/router';
import { ComponentRouterModule } from 'ng-routing-utils';

@NgModule({
  declarations: [AuthComponent],
  providers: [AuthService, AuthGuard],
  imports: [
    CommonModule,
    HttpClientModule,
    ComponentRouterModule.forFeature({
      root: {
        path: 'auth',
      },
      components: [AuthComponent],
    }),
  ],
  exports: [RouterModule],
})
export class AuthModule {}
