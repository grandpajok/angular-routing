import { Component, OnInit } from '@angular/core';
import { Route } from 'ng-routing-utils';
import { AuthService } from './auth.service';

@Route()
@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
  providers: [AuthService],
})
export class AuthComponent implements OnInit {
  constructor(private authService: AuthService) {}

  ngOnInit(): void {
    console.log(this.authService.test());
  }
}
