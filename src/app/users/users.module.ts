import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './users.component';
import { ComponentRouterModule } from 'ng-routing-utils';
import { RouterModule } from '@angular/router';
import { DetailsComponent } from './details/details.component';

@NgModule({
  declarations: [UsersComponent, DetailsComponent],
  imports: [
    CommonModule,
    ComponentRouterModule.forFeature({
      root: {
        path: 'users',
      },
      components: [UsersComponent, DetailsComponent],
    }),
  ],
  exports: [RouterModule],
})
export class UsersModule {}
