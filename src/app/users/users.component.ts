import { Component, OnInit } from '@angular/core';
import { Route } from 'ng-routing-utils';
import { ActivatedRoute } from '@angular/router';

@Route()
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  forbiddenId!: any;

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      if (params.forbidden) {
        this.forbiddenId = params.forbidden;
      }
    });
  }
}
