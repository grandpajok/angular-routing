import { Component, OnInit } from '@angular/core';
import { Route } from 'ng-routing-utils';
import { ActivatedRoute, Router } from '@angular/router';

@Route(':id/details')
@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
})
export class DetailsComponent implements OnInit {
  userId: number = 0;

  constructor(private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      const id = Number(params.id);
      if (!id) {
        this.router.navigate(['users'], {
          queryParams: {
            forbidden: params.id,
          },
          replaceUrl: true,
        });
      }
      this.userId = id;
    });
  }
}
